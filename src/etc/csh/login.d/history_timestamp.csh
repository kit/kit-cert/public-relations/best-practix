# Not tcsh?
if ( ! ${?tcsh} ) exit 0

# Not an interactive shell?
if ( ! ${?prompt} ) exit 0

# Set the history file format
set history=(2000 "%h %Y-%W-%DT%P %R\n")

# Make history permanent
set savehist=(2000 merge)
