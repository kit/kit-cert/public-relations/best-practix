# Not root?
if ( ${uid} != 0 ) exit 0

# Not tcsh?
if ( ! ${?tcsh} ) exit 0

# Not an interactive shell?
if ( ! ${?prompt} ) exit 0

# Set the prompt
set prompt='\n%Y-%W-%DT%P (%h)\n%n@%m:%~%# '
