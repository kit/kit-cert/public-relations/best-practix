# SSH Server

## Sensible configuration settings in `/etc/ssh/sshd_config`

Note: some distributions may put `sshd_config` someplace else. When in doubt, consult your distribution's documentation and/or package manager.

* For OpenSSH <6.3 set `LogLevel VERBOSE` to log the public key hash / certificate of logged in users. Newer releases log these by default.

### Authentication schemes

* Don't authenticate user's based on who the other party claims to be: `HostbasedAuthentication no`
* Don't let `root` login using passwords: `PermitRootLogin without-password`
* When possible, use passwordless authentication schemes.
* When allowing password authentication (`PasswordAuthentication yes`), don't accept empty passwords: `PermitEmptyPasswords no`

### Encryption parameters

Disable DSA hostkeys (comment out the `HostKey` line that references the DSA key).

Ciphersuite support differs between different versions of OpenSSH. Please refer to the »applied crypto hardening« handbook from bettercrypto.org to find sensible settings for your setup.

Here's a set of settings that works on Debian 7/wheezy (OpenSSH 6.0p1):
```
Ciphers aes256-ctr,aes128-ctr
MACs hmac-sha2-512,hmac-sha2-256,hmac-ripemd160
KexAlgorithms diffie-hellman-group-exchange-sha256,diffie-hellman-group14-sha1,diffie-hellman-group-exchange-sha1
```

OpenSSH >=6.6:

```
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes128-ctr
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,hmac-ripemd160
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256,diffie-hellman-group14-sha1,diffie-hellman-group-exchange-sha1
```

### misc

* You may optionally restrict the list of environment variables sent by the client that will be copied into the session's environment. Here's an exemplary list of
  locale-related settings:
```
AcceptEnv LANG LC_*
```
* Set `UsePrivilegeSeparation` to `sandbox` if your openssh version supports that.

## Special use case: SFTP-only access

To create special users that may only use SSH to exchange files via sftp, create the account(s) with a non-login shell like `/sbin/nologin`.
We also suggest to create a special group to make it easy to add and remove accounts:

```
GROUPNAME=sftpaccess
USERNAME=filetransfer
groupadd --system "${GROUPNAME}"
useradd -c "sftp only group" -G "${GROUPNAME}" --create-home --system --shell /bin/false "${USERNAME}"
```

Now add the following snippet to `sshd_config` and customize it to fit your environment:

```
Match Group sftpaccess
    ForceCommand internal-sftp
    AllowAgentForwarding no
    AllowTcpForwarding no
    PermitTunnel no
    X11Forwarding no
```

To log every user's actions, change the second line into `ForceCommand internal-sftp -l VERBOSE`.

Use `ChrootDirectory` to further isolate these accounts. Attention: setting `ChrootDirectory` to `%h` allows
users to add or remove ssh-keys to their account unless `~/.ssh/authorized_keys` is proteced by changing ownership and
removing write-permissions. `ChrootDirectory` requires certain restrictions upon ownership and permissions of the
chroot directory; please see the sshd manpage for further information.
