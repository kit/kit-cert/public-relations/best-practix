# SSH-Server

## Sensible configuration settings in `/etc/ssh/sshd_config`

Note: some distributions may put `sshd_config` someplace else. When in doubt, consult your distribution's documentation and/or package manager.

* For OpenSSH <6.3 set `LogLevel VERBOSE` to log the public key hash / certificate of logged in users. Newer releases log these by default.

### Authentication schemes

* Don't authenticate user's based on who the other party claims to be: `HostbasedAuthentication no`
* Don't let `root` login using passwords: `PermitRootLogin without-password`
* When possible, use passwordless authentication schemes.
* When allowing password authentication (`PasswordAuthentication yes`), don't accept empty passwords: `PermitEmptyPasswords no`

### Encryption parameters

Disable DSA hostkeys (comment out the `HostKey` line that references the DSA key). If possible, disable RSA and ECDSA and only offer ED25519.

Ciphersuite support differs between different versions of OpenSSH. Please refer to the [»applied crypto hardening« handbook](https://bettercrypto.org/static/applied-crypto-hardening.pdf) from [bettercrypto.org](https://bettercrypto.org/) to find sensible settings for your setup.

Here's a set of settings that works on OpenSSH 6.7p1 (Debian 8 Jessie):
```
KexAlgorithms curve25519-sha256@libssh.org
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com
```

OpenSSH >=6.6:

```
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes128-ctr
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,hmac-ripemd160
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256,diffie-hellman-group14-sha1,diffie-hellman-group-exchange-sha1
```

Use [ssh-audit](https://github.com/arthepsy/ssh-audit) to better tune these parameters.

### Prevent bruteforce login attempts

There are several ways to prevent other hosts from repeatedly trying to guess
passwords via ssh. We'll briefly discuss some approaches so the administrator
can choose the most suitable one:

#### iptables

Add iptables rules that block hosts if they exceed a predefined rate of
connections per timespan. This approach cannot distinguish between successful
and unsuccessful login attempts. Whitelisting hosts is pretty easy.

Example
([source](https://rudd-o.com/linux-and-free-software/a-better-way-to-block-brute-force-attacks-on-your-ssh-server)),
add whitelisting rules in front and change interface name accordingly:

```
-A INPUT -i eth0 -p tcp -m tcp --dport 22 -m state --state NEW -m recent --set --name SSH --rsource
-A INPUT -i eth0 -p tcp -m tcp --dport 22 -m recent --rcheck --seconds 30 --hitcount 4 --rttl --name SSH --rsource -j REJECT --reject-with tcp-reset
-A INPUT -i eth0 -p tcp -m tcp --dport 22 -m recent --rcheck --seconds 30 --hitcount 3 --rttl --name SSH --rsource -j LOG --log-prefix "SSH brute force "
-A INPUT -i eth0 -p tcp -m tcp --dport 22 -m recent --update --seconds 30 --hitcount 3 --rttl --name SSH --rsource -j REJECT --reject-with tcp-reset
-A INPUT -i eth0 -p tcp -m tcp --dport 22 -j ACCEPT
```

If you use iptables wrapper like firewalld, consult the proper documentation on how to do this.

#### fail2ban

Software like [fail2ban](http://www.fail2ban.org/) monitors logfiles to
identify and block offending IP addresses using various mechanisms
(tcpwrappers, iptables, …). It can distinguish between failed und successful
logins and allows for easy whitelisting. Very high connection loads may lead to
a denial of service because fail2ban has to parse loads of
logdata.Configuration differs between different versions and distributions,
please refer to the local documentation and examples.

#### Obfuscation

This includes techniques like [port
knocking](https://en.wikipedia.org/wiki/Port_knocking) and using different tcp
ports (port 24 seems to be a popular choice for sshd). We advise against these
tactics for two reasons. First of all: these techniques are not very user
friendly. Secondly, they are in direct violation of [Kerckhoffs's
principle](https://en.wikipedia.org/wiki/Kerckhoffs%27s_principle) and thus not
suitable as an effective security mechanism.

### misc

* You may optionally restrict the list of environment variables sent by the client that will be copied into the session's environment. Here's an exemplary list of
  locale-related settings:
```
AcceptEnv LANG LC_*
```
* Set `UsePrivilegeSeparation` to `sandbox` if your openssh version supports that.

## Special use case: SFTP-only access

To create special users that may only use SSH to exchange files via sftp, create the account(s) with a non-login shell like `/sbin/nologin`.
We also suggest to create a special group to make it easy to add and remove accounts:

```
GROUPNAME=sftpaccess
USERNAME=filetransfer
groupadd --system "${GROUPNAME}"
useradd -c "sftp only group" -G "${GROUPNAME}" --create-home --system --shell /bin/false "${USERNAME}"
```

Now add the following snippet to `sshd_config` and customize it to fit your environment:

```
Match Group sftpaccess
    ForceCommand internal-sftp
    AllowAgentForwarding no
    AllowTcpForwarding no
    PermitTunnel no
    X11Forwarding no
```

To log every user's actions, change the second line into `ForceCommand internal-sftp -l VERBOSE`.

Use `ChrootDirectory` to further isolate these accounts. Attention: setting `ChrootDirectory` to `%h` allows
users to add or remove ssh-keys to their account unless `~/.ssh/authorized_keys` is proteced by changing ownership and
removing write-permissions. `ChrootDirectory` requires certain restrictions upon ownership and permissions of the
chroot directory; please see the sshd manpage for further information.

# SSH-Client

## sensible security settings

* avoid using protocol version 1
* only allow secure key exchange, cipher and MAC algorithms (see bettercrypto.org for instance)
* use agent forwarding with extreme care
* only use RSA and ED25519 keypairs
* Using 2048bit RSA keys is fine for now, create new keys with at least 4096bit

```
# ssh_config
Host *
  Protocol = 2
  KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
  Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctrMACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-ripemd160-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,hmac-ripemd160,umac-128@openssh.com
  MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-ripemd160-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,hmac-ripemd160,umac-128@openssh.com
```


## SSH key agents

SSH key agents allow to use a strong passphrase to protect your private key
without the need to retype said passphrase on every new connection.
Only use unencrypted keys for automated tasks and restrict their abilities on
the server side (e.g. SSH forced commands, restricted shells).


## Use the new private key format for OpenSSH >=6.5

To get an encryption key from a given passphrase older OpenSSH versions just hashed
the password with MD5. This is a relatively cheap operation that might make a
brute-force attack feasible in case you lose your private key and do not use a
strong passphrase. Beginning with 6.5 OpenSSH supports a new private key format
that uses PBKDF to derive the encryption key from the passphrase. This format
is the default for ED25519 key pairs and can be used for other formats as well
with `ssh-keygen -o`.

PBKDF runs 16 rounds by default, which can be increased to slow down brute force
attackers even further. The switch `-a` specifies the number of rounds the KDF
runs. On decent hardware 100 rounds should work fine.

```
# Creating a new RSA key with 100 rounds bcrypt PBKDF
ssh-keygen -o -a 100 -t rsa -b 4096 -f my_new_rsa_key -C "Key Description (generated $(date +%F ) by $(whoami))" 

# Converting an existing key to the new format
ssh-keygen -o -a 100 -f ~/.ssh/id_rsa -p
```

ED25519 keys automatically use the new format. Be aware that older OpenSSH
servers and dropbear do not support it. PuTTY [added support for ED25519
keys](http://www.chiark.greenend.org.uk/~sgtatham/putty/wishlist/ed25519.html)
in 2015.


```
ssh-keygen -t ed25519 -f my_new_ed25519_key -C "Key Description (generated $(date +%F ) by $(whoami))" 
```


## Use SSH multiplexing to improve login latency

* `ControlPath` allows multiplexing SSH connections
* authorize once and open subsequent connections over the existing channel
* `ControlPersist` allows to cache open connections for a given amount of time
* improves subsequent login speed by an order of magnitude

```
mkdir ~/.ssh/cp
chmod 0700 ~/.ssh/cp
```

```
# ssh_config
Host *
  ControlMaster auto
  ControlPath ~/.ssh/cp/%r@%h:%p
  ControlPersist 5m
```