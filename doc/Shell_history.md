Enhance Your Shell History
==========================

It is advisable to have your shell record timestamps in its history file and
make the history persistent.

To enable this, deploy /etc/profile.d/history_timestamp.sh for bash/zsh and
/etc/csh/login.d/history_timestamp.csh for tcsh.

Neither csh nor ksh support this.
