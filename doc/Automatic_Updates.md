# Automatic Updates

To determine distribution and release version, try ```lsb_release -a``` or ```cat /etc/*{_version,-release}```.

## Debian-based distros (Debian, Ubuntu)

```sh
apt-get -y install unattended-upgrades cron-apt-
# set to 'yes'
dpkg-reconfigure -plow unattended-upgrades
# disable non-security-updates, change e-mail address
sed -i -e '/stable/ s/^/\/\//' \
  -e '/Unattended-Upgrade::Mail/ s/^/\/\//' \
  -e '/Unattended-Upgrade::Mail/ a Unattended-Upgrade::Mail "INSERT.YOUR@MAIL.ADDRESS.HERE";' \
  /etc/apt/apt.conf.d/50unattended-upgrades
```

See also:
* [UnattendedUpgrades](https://wiki.debian.org/UnattendedUpgrades)
* [Automatic Updates](https://help.ubuntu.com/lts/serverguide/automatic-updates.html)

## Redhat-based distros (RHEL, CentOS, Scientific Linux) > v5

```sh
yum install -y yum-plugin-security yum-utils.noarch yum-cron
# enable only security-relevant updates, change e-mail address
sed -i -e 's/^MAILTO=$/MAILTO="INSERT.YOUR@MAIL.ADDRESS.HERE"/' \
  -e 's/^YUM_PARAMETER=$/YUM_PARAMETER="--security"/' \
  /etc/sysconfig/yum-cron
# CentOS 7 benutzt /etc/yum/yum-cron.conf
chkconfig yum-cron on
service yum-cron start
```

See also:
* [Updates on RHEL/CentOS/SL 5](https://www.centos.org/docs/5/html/yum/sn-updating-your-system.html)


## Fedora

Starting with release 21, Fedora uses [dnf](http://dnf.readthedocs.org) instead of yum.

```sh
dnf install -y dnf-automatic
# enable automatic updates
sed -i -e '/apply_updates\s*=/ s/no/yes/' /etc/dnf/automatic.conf
# optional: install only security patches
sed -i -e '/upgrade_type\s*=/ s/default/security/' /etc/dnf/automatic.conf
# enable dnf-automatic
systemctl enable dnf-automatic.timer
systemctl start dnf-automatic.timer
```

See also:
* [AutoUpdates](https://fedoraproject.org/wiki/AutoUpdates)

## misc

* ATIS uses [apt-dater](http://www.ibh.de/apt-dater/) for massive parallel remote updates
* TODO: Use [ansible playbooks](http://docs.ansible.com/ansible/) for parallel manual upgrades