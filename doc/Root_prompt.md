Enhance Your Root Prompt
========================

It is generally useful to have your root prompt include key information, at least
 * Timestamp
 * History event ID
 * Username
 * Hostname
 * Current working directory

Additionally, if possible, it is useful to colorize your root prompt to make it
obvious that this is a root prompt.  However, under certain circumstances such
as having really really old terminals this breaks things.

To add the above information, deploy /etc/profile.d/timestamped_root_prompt.sh
if your root shell as either a bash or a zsh or
/etc/csh/login.d/timestamped_root_prompt.csh if your root shell is a tcsh.

To have the root prompt colorized as well, deploy
/etc/profile.d/colorized_root_prompt.sh or
/etc/csh/login.d/colorized_root_prompt.csh instead.

If you are using a bash or a zsh, you also have to append /root/bashrc.appendage
to /root/.bashrc or /root/zshrc.appendage to /root/.zshrc, respectively, because
the root prompt is redefined after the profile scripts have been executed.

If you are using an original csh or ksh, there is little you can do (except
upgrade to a real shell, of course ;-)).
