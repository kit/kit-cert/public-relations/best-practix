Good Ideas
==========

* Replace `/usr/bin/passwd` with the script in `src`, renaming the original
  `passwd` to `passwd.orig`.

* Replace `/usr/bin/shred` with the script in `src`, renaming the original
  `shred` to `shred.orig`.

* Disallow root login except with SSH keys.

* Tag (root) SSH keys with names and log which key is used to log in.

* Add timestamping to root shell logs (histfile and such) -> See Shell_history.md

* Add timestamping to root shell prompt (PS1). -> See Root_prompt.md

* Add colorization to root shell prompt (PS1). -> See Root_prompt.md

* Remote logging.

* Use chkroot, tripwire, and the like.

* Analyze logs with logwatch etc.

* Keep track of setuid binaries.

* Regularly verify that the SSH binary actually running is the same as the
  binary on disk.

* Mount filesystems with noexec and nodev whenever possible (particularly
  /tmp and /var).

* etckeeper

* Regularly check for existence of things like /tmp/... and so on.