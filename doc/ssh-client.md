# Using OpenSSH securely at KIT

## Introduction

Secure Shell (SSH) is a protocol suite to run cryptographically secure network
services on an unsecured network. Its most common use cases are remote login
to a shell and file transfers. While there are more advanced features of SSH,
they are beyond the scope of this document.

The recommendations below are intended for end users accessing central SCC
services via SSH, such as GridKa, HPC, the central web server cluster, or the 
KIT GitLab instance.

The goal is a secure configuration without compromising too much convenience.
Advanced users may object to some of the recommendations.

This document assumes at least [OpenSSH](https://www.openssh.com/) version 7.3
on the client side, which was released in August of 2016. Older versions do not
support some of the features required for the recommendations below. The
configuration may also be incompatible with alternative SSH server
implementations like tinyssh or dropbear.

The OpenSSH client is included in all major operating systems. The following
lists the minimal release for popular operating systems, which should work
out-of-the-box with this guide: 

* Windows 10 / Server 2019 1809 aka "April 2018 Update"
* macOS 10.12+
* RedHat/CentOS/Scientific Linux 7+
* Ubuntu 18.04+
* Debian 9+
* SLES 15+.

To check the installed version, open a terminal window and run `ssh -V`.

If your system ships with an older version, you should consider an operating
system upgrade or talk to the person responsible for managing your computer.

---
**Recommendation 1: Keep your computer up-to-date**

[Installing the provided operating system patches by the vendor](www.scc.kit.edu/sl/patch)
is the single most important aspect of maintaining computer security.

---


## Getting started

SSH normally uses a trust-on-first-use security model. This means, when you
connect to a remote host for the very first time on any computer, the client
lets you decide to trust the remote server by showing a prompt like below:

```
$ ssh example.kit.edu
The authenticity of host 'example.kit.edu (2a01:4f8:221:2f83:2080::2)' can't be established.
ECDSA key fingerprint is SHA256:9ukznkqicxJ+khNikxeAgkAfHm87CHrEyqCpHrHQYjg.
Are you sure you want to continue connecting (yes/no/[fingerprint])?

```

If the remote server's fingerprint is documented publicly, e.g. on a website,
you can manually verify the correctness.  If you accept the fingerprint with
'yes', the client will store the fingerprint locally and subsequent connections
will only succeed if the fingerprint does not change.

Should you ever see the warning below on a centrally managed system, please
contact the administrator of the remote system. 

```
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:9ukznkqicxJ+kwNikxeAgkAfHm07CHrEyqCpHrHQYjg.
Please contact your system administrator.
Add correct host key in /home/example/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /home/example/.ssh/known_hosts:14
ECDSA host key for example.kit.edu has changed and you have requested strict checking.
Host key verification failed.
```

You should now be able to login to the remote server with your username and password. 


```
$ ssh ab1234@example.kit.edu
ab1234@example.kit.edu's password:
```

Some service providers may prohibit login with username and password for
security reasons. In that case, follow the steps below to create a key pair and
follow the provider's documentation to upload the public key to their system.


## Creating a key pair

To create your first key pair, run

```
ssh-keygen -t ed25519
```

Accept the default file name and pick a secure passphrase. If in doubt, follow
the KIT password policy ([password generator](https://www.cert.kit.edu/p/PasswordGenerator)).
Do not re-use a password from another system.

There are advanced use cases for unencrypted keys for automation tasks or
machine-to-machine communication. Only ever use them with
[```ForceCommand```](https://man.openbsd.org/sshd_config.5#ForceCommand) or in
restricted shells. Their usage may be administratively prohibited for specific
environments (e.g. HPC, GridKa).

A keypair consists of two different keys: the private (secret) key which remains
on your system and the public key which is installed on the remote system.

Next, upload the public key to the SSH server:

```
ssh-copy-id user@server
```

Next time, you login to the remote server, you should be asked for your private
key passphrase instead of your password.


FIXME: Bild


---
**Recommendation 2: Protect your SSH private keys**

Always encrypt your private keys with a strong passphrase.  Keep your private
keys secure. Ideally, they should never leave your personal system (with the
exception of backups).

---


## Use ssh-agent


SSH key managers allow toi use private keys with strong passphrases and avoid
typing the passphrase on every connection.

FIXME: Start service on Windows [MS-Docs](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement)

FIXME: Use system key managers (Keychain, gnome-keyring, keychain, pageant+ssh-pageant)



---
**Recommendation 3: Protect your `ssh-agent`**

Do not forward your agent to remote systems. 

---


## File transfers with SSH

The standard utility to copy files to and from remote systems is `scp`. The 
protocol has some design issues and the OpenSSH maintainers recommend to use
alternatives.

---
**Recommendation 4: Replace `scp` with modern alternatives**

---

## Create and store private keys on hardware devices

FIXME (only recommend FIDO2 with OpenSSH 8.2?)
